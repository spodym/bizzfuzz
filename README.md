# bizzfuzz

## Installation

```bash
mkvirtualenv --python=`which python3` bizzfuzz
pip install -r requirements
cd bizzfuzz
./manage.py migrate
```

## Run server

```bash
./manage.py runserver
```

## Run tests

```bash
./manage.py test
```

## Notes

I have modified task no 4. Because I started with DRF template tags would not work as expected. DRF view works on serialized object and I assume task was to create template tag working on User object. Instead I continued with DRF and serialization of User properties. I have created template tags but those were not used. Beside those template tags are just wrappers around user properties - those can be used directly in templates.

All other tasks (including optional) were done.

## Screen shots

![List view](/img/list_view.png)
![Detail view](/img/detail_view.png)
![CSV panel](/img/csv_panel.png)

# earliest

```bash
python earliest.py example1
pytest test.py
```
