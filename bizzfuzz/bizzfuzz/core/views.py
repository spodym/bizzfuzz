from rest_framework import viewsets
from rest_framework.settings import api_settings

from bizzfuzz.core.models import User
from bizzfuzz.core.serializers import UserSerializer
from bizzfuzz.core.renderers import UserCSVRenderer


class UserViewSet(viewsets.ModelViewSet):
    renderer_classes = api_settings.DEFAULT_RENDERER_CLASSES + [UserCSVRenderer,]
    queryset = User.objects.all()
    serializer_class = UserSerializer
