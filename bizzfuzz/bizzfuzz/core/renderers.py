from rest_framework_csv import renderers as r


class UserCSVRenderer(r.CSVRenderer):
    header = [
        'username', 'birth_date', 'age_restriction', 'random_num', 'bizzfuzz']
    labels = {
        'username': 'Username',
        'birth_date': 'Birthday',
        'age_restriction': 'Eligible',
        'random_num': 'Random Number',
        'bizzfuzz': 'BizzFuzz',
    }
