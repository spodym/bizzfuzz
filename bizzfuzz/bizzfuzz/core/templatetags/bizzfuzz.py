import datetime
from django import template

register = template.Library()


@register.simple_tag
def bizzfuzz(user):
    return user.bizzfuzz
