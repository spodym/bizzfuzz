import datetime
from django import template

register = template.Library()


@register.simple_tag
def age_restriction(user):
    return user.age_restriction
