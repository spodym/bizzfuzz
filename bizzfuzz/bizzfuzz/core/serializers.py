from rest_framework import serializers

from bizzfuzz.core.models import User


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = (
            'url', 'id', 'username', 'birth_date', 'random_num',
            'age_restriction', 'bizzfuzz')
        read_only_fields = ('random_num', 'age_restriction', 'bizzfuzz')
