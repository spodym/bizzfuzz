import datetime
import random

from django.db import models
from django.contrib.auth.models import AbstractUser

AGE_THRESHOLD = 13


def randint():
    return random.randint(1,100)


class User(AbstractUser):
    birth_date = models.DateField(null=True, blank=True)
    random_num = models.IntegerField(default=randint)

    @property
    def age_restriction(self):
        if self.birth_date:
            today = datetime.date.today()
            threshold = datetime.date(today.year - AGE_THRESHOLD, today.month, today.day)

            if self.birth_date <= threshold:
                return 'allowed'

        return 'blocked'

    @property
    def bizzfuzz(self):
        div5 = self.random_num % 5 == 0
        div3 = self.random_num % 3 == 0

        if div3 and div5:
            return 'BizzFuzz'
        elif div3:
            return 'Bizz'
        elif div5:
            return 'Fuzz'

        return self.random_num
