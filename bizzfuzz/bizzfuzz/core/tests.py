import datetime

from django.utils import timezone
from django.test import TestCase
from freezegun import freeze_time

from bizzfuzz.core.models import User


class UserModelTests(TestCase):

    @freeze_time("2013-05-05 12:00:00")
    def test_age_restriction(self):
        u = User(birth_date=datetime.date(2012, 1, 1))
        self.assertEqual(u.age_restriction, 'blocked')

        u = User(birth_date=datetime.date(1012, 1, 1))
        self.assertEqual(u.age_restriction, 'allowed')

        u = User(birth_date=datetime.date(2000, 5, 6))
        self.assertEqual(u.age_restriction, 'blocked')

        u = User(birth_date=datetime.date(2000, 5, 5))
        self.assertEqual(u.age_restriction, 'allowed')

    def test_bizzfuzz(self):
        u = User(random_num=3)
        self.assertEqual(u.bizzfuzz, 'Bizz')

        u = User(random_num=5)
        self.assertEqual(u.bizzfuzz, 'Fuzz')

        u = User(random_num=15)
        self.assertEqual(u.bizzfuzz, 'BizzFuzz')

        u = User(random_num=92)
        self.assertEqual(u.bizzfuzz, 92)
