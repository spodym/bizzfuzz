import argparse
import datetime
from itertools import permutations


def read_date_in(filename):
    date = None
    with open(filename) as file:
        date = file.readline()

    return date


def format_date(date):
    if date[0] < 100:
        date = (date[0] + 2000, date[1], date[2])
    return date


def is_valid_date(date):
    try:
        datetime.date(*date)
        return True
    except ValueError:
        return False


def earliest(datein):
    """Core function of problem.

    Explanation:
    For every permutation of date parts algorithm checks if it is valid date in
    datetime module. First valid permutation is result because permutations are
    sorted.

    Even though year in 4 character format will not be first - every solution
    before will be invalid (no month or day can have value >2000).
    """
    date = None
    for newdate in sorted(permutations(datein, 3)):
        newdate = format_date(newdate)
        if is_valid_date(newdate):
            # Format: year-month-day
            date = "{}-{}-{}".format(*newdate)
            break

    return date


def process(date_in):
    date_in = [int(x.strip()) for x in date_in.split('/')]

    if date_in:
        date = earliest(date_in)
        if date:
            return date

    return 'is illegal'


def main():
    parser = argparse.ArgumentParser(
        description='Find earliest valid date')
    parser.add_argument('filename', nargs=1, help='Input file')
    args = parser.parse_args()

    date_in = read_date_in(args.filename[0])
    res = process(date_in)
    print(res)


if __name__ == '__main__':
    main()
