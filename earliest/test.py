import random
from itertools import permutations

from earliest import earliest, process


def test_simple_dates():
    assert earliest((1,2,3)) == '2001-2-3'
    assert earliest((3,20,1)) == '2001-3-20'
    assert earliest((2,2,2001)) == '2001-2-2'
    assert earliest((2,2,1)) == '2001-2-2'
    assert earliest((2,4,1)) == '2001-2-4'


def test_dates_format():
    assert process('1/2/3') == '2001-2-3'
    assert process('3/20/1') == '2001-3-20'
    assert process('03/20/1') == '2001-3-20'
    assert process('3/20/01') == '2001-3-20'


def test_permutations():
    for r in [10,30,50,99]:
        for _ in range(1000):
            date = (random.randint(0,r) for _ in range(3))

            resp = earliest(date)

            # Every permutation should give same answer
            for p in permutations(date, 3):
                assert earliest(p) == resp


def test_permutations_long_year():
    for r in [10,30,50,99]:
        for _ in range(1000):
            date = (
                random.randint(0,r),
                random.randint(0,r),
                random.randint(2000,2999),
            )

            resp = earliest(date)

            # Every permutation should give same answer
            for p in permutations(date, 3):
                assert earliest(p) == resp

